import React from 'react';
import PropTypes from 'prop-types';

const Cell = ({cellClass, id}) => <div className={cellClass} id={id} />;

Cell.propTypes = {
    cellClass: PropTypes.string,
    id: PropTypes.string
};

export default Cell;