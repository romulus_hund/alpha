import React from 'react';
import Cell from './Cell';
import PropTypes from 'prop-types';

const Board = ({cellsPerRow, board}) => {

    let rowsArr = [];

    let cellClass = '';

    for (let i = 0; i < cellsPerRow; i++) {
        for (let j = 0; j < cellsPerRow; j++) {
            let cellId = `${i}-${j}`;

            cellClass = board[i][j] ? 'cell live' : 'cell dead';

            rowsArr.push(
                <Cell
                    cellClass={cellClass}
                    key={cellId}
                    cellId={cellId}
                />
            );
        }
    }

    return (
        <div className="grid" >
            {rowsArr}
        </div>
    );

};

Board.propTypes = {
    rows: PropTypes.number,
    cols: PropTypes.number,
    board: PropTypes.array
}

export default Board;