import React, {useEffect} from 'react';
import Board from './components/Board';
import {getBoard} from './store/selectors';
import {setBoard} from './store/actions/boardActions';
import {connect} from 'react-redux';

const cellsPerRow = 50;

const App = ({board, setBoard}) => {

  useEffect(() => {
    initGame();
  }, []);

  const initGame = () => {
    // clearInterval(this.intervalId);
   setInterval(() => {
      let boardCopy = Object.assign(board, []);

      for (let i = 0; i < cellsPerRow; i++) {
        for (let j = 0; j < cellsPerRow; j++) {
          let count = 0;
          if (i > 0){
            if (board[i - 1][j]){
              count++;
            }
          }

          if (i > 0 && j > 0) {
            if (board[i - 1][j - 1]){
              count++;
            }
          }

          if (i > 0 && j < cellsPerRow - 1){
            if (board[i - 1][j + 1]) {
              count++;
            }
          }

          if (j < cellsPerRow - 1){
            if (board[i][j + 1]){
              count++;
            }
          }
          if (j > 0){
            if (board[i][j - 1]) {
              count++;
            }
          }

          if (i < cellsPerRow - 1){
            if (board[i + 1][j]){
              count++;
            }
          }

          if (i < cellsPerRow - 1 && j > 0){
            if (board[i + 1][j - 1]) {
              count++;
            }
          }

          if (i < cellsPerRow - 1 && j < cellsPerRow - 1) {
            if (board[i + 1][j + 1]){
              count++;
            }
          }

          if (board[i][j] && (count < 2 || count > 3)){
            boardCopy[i][j] = 0;
          }

          if (!board[i][j] && count === 3) {
            boardCopy[i][j] = 1;
          }
        }
      }

      setBoard(boardCopy);
    }, 400);
  };
  
    return (
        <div>
          <Board
              board={board}
              cellsPerRow={cellsPerRow}
          />
        </div>
    );

};

const mapStateToProps = state => {
  return {
    board: getBoard(state)
  };
};

function mapDispatchToProps(dispatch) {
  return {
    setBoard: data => dispatch(setBoard(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
