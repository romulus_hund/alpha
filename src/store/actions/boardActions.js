import types from '../actionsTypes';

export const setBoard = data =>{
    return{
        type: types.SET_BOARD,
        payload: data
    };
};

