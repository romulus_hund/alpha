import actionsTypes from '../actionsTypes';
import createReducer from '../reducers/createReducer';

const cellsPerRow = 50;

const createBoard = () => {
    const rows = [];
    for(let i = 0; i < cellsPerRow; i++){
        rows.push(Array.from(Array(cellsPerRow), ()=> 0));
    }

    return rows;
};

const populateBoard = () => {
    let boardCopy = Object.assign(createBoard(), []);
    for (let i = 0; i < cellsPerRow; i++) {
        for (let j = 0; j < cellsPerRow; j++) {

            // random choose 1 or 0
            if (Math.round(Math.random() ) === 1) {
                boardCopy[i][j] = 1;
            }
        }
    }
    return boardCopy
};

const initialState = {
    board: populateBoard()
};

const boardReducer = createReducer(initialState, {
    [actionsTypes.SET_BOARD]: (state, {payload}) => {

        return {
            ...state,
            board: [
                ...payload
            ]
        };
    }

});

export default boardReducer;

