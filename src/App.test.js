
const cellsPerRow = 3;

const createBoard = () => {
  const rows = [];
  for(let i = 0; i < cellsPerRow; i++){
    rows.push(Array.from(Array(cellsPerRow), ()=> 0));
  }

  return rows;
};

const  populateBoard = () => {
  let boardCopy = Object.assign(createBoard(), []);
  for (let i = 0; i < cellsPerRow; i++) {
    for (let j = 0; j < cellsPerRow; j++) {

      // random choose 1 or 0
      if (Math.round(Math.random() ) === 1) {
        boardCopy[i][j] = 1;
      }
    }
  }
  return boardCopy
}

const  playGame = (board) => {

  let boardCopy = Object.assign(board, []);

  for (let i = 0; i < cellsPerRow; i++) {
    for (let j = 0; j < cellsPerRow; j++) {
      let count = 0;
      if (i > 0){
        if (board[i - 1][j]){
          count++;
        }
      }

      if (i > 0 && j > 0) {
        if (board[i - 1][j - 1]){
          count++;
        }
      }

      if (i > 0 && j < cellsPerRow - 1){
        if (board[i - 1][j + 1]) {
          count++;
        }
      }

      if (j < cellsPerRow - 1){
        if (board[i][j + 1]){
          count++;
        }
      }
      if (j > 0){
        if (board[i][j - 1]) {
          count++;
        }
      }

      if (i < cellsPerRow - 1){
        if (board[i + 1][j]){
          count++;
        }
      }

      if (i < cellsPerRow - 1 && j > 0){
        if (board[i + 1][j - 1]) {
          count++;
        }
      }

      if (i < cellsPerRow - 1 && j < cellsPerRow - 1) {
        if (board[i + 1][j + 1]){
          count++;
        }
      }

      if (board[i][j] && (count < 2 || count > 3)){
        boardCopy[i][j] = 0;
      }

      if (!board[i][j] && count === 3) {
        boardCopy[i][j] = 1;
      }
    }
  }

  return boardCopy
}

it('boardCopy produces the same board', () => {
  const board = populateBoard();
  const newBoard = Object.assign(board, [])
  expect(board).toEqual(expect.arrayContaining(newBoard));
});

it('creates new board with right default values of 0 for every cell', () => {
  const board = createBoard();
  expect([[0, 0, 0], [0, 0, 0], [0, 0, 0]]).toEqual(expect.arrayContaining(board));
});

it('cell with two or three live neighbours lives on to the next generation', () => {
  let board = playGame([[0, 0, 0, 0], [0, 0, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]])
  expect([[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]).toEqual(expect.arrayContaining(board));
})

it('live cell with fewer than two live neighbours dies', () => {
  let board = playGame([[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]])
  expect(board[0][0]).toEqual(0);
})

